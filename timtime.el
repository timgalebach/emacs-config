;;;;TIMSTUFF
(global-set-key "\C-x\C-m" 'execute-extended-command)

(when (>= emacs-major-version 24)
  (require 'package)
  (package-initialize)
  (add-to-list 'package-archives
               '("melpa" . "https://melpa.org/packages/")))


;;indent the fuck out of everything
(defun indent-buffer ()
  (interactive)
  (save-excursion
    (indent-region (point-min) (point-max) nil)))
(global-set-key [f12] 'indent-buffer)

;;multi-web-mode
(require 'multi-web-mode)
(setq mweb-default-major-mode 'html-mode)
(setq mweb-tags '((php-mode "<\\?php\\|<\\? \\|<\\?=" "\\?>")
                  (js-mode "<script +\\(type=\"text/javascript\"\\|language=\"javascript\"\\)[^>]*>" "</script>")
                  (css-mode "<style +type=\"text/css\"[^>]*>" "</style>")))
(setq mweb-filename-extensions '("php" "htm" "html" "ctp" "phtml" "php4" "php5"))
(multi-web-global-mode 1)

;;emmet
(add-hook 'sgml-mode-hook 'emmet-mode) ;; Auto-start on any markup modes
(add-hook 'css-mode-hook  'emmet-mode) ;; enable Emmet's css abbreviation.
(setq emmet-move-cursor-between-quotes t) ;; default nil

;;AVY, get rid of ace-jump (disabled currently)
;;(global-set-key (kbd "C-o") 'avy-goto-char)

;;js2
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))

;;Cider
(setq nrepl-log-messages t)
(setq nrepl-hide-special-buffers t)
(setq cider-auto-select-error-buffer nil)
(add-hook 'cider-repl-mode-hook 'paredit-mode)

;;purescript -- forall, colon, and pointer key
(global-set-key (kbd "C-c a") (lambda () (interactive) (insert "∀")))
(global-set-key (kbd "C-c p") (lambda () (interactive) (insert "→")))
(global-set-key (kbd "C-c o") (lambda () (interactive) (insert "←")))
(global-set-key (kbd "C-c c") (lambda () (interactive) (insert "∷")))
(global-set-key (kbd "C-c v") (lambda () (interactive) (insert "∘")))
(global-set-key (kbd "C-c x") (lambda () (interactive) (insert "⊕")))
(global-set-key (kbd "C-c t") (lambda () (interactive) (insert "×")))

(add-to-list 'load-path "/Users/blah/usr/lib/emacs/purescript-mode/")
(require 'purescript-mode-autoloads)
(add-hook 'purescript-mode-hook #'haskell-indentation-mode)

;;Clojure
(defun cider-init-system ()
  (cider-repl-set-ns "user"))

(defun cider-repl-reset ()
  (cider-switch-to-current-repl-buffer)
  (goto-char (point-max))
  (insert "(reset)")
  (cider-repl-return))

(global-set-key (kbd "C-c r") (lambda () (interactive) (cider-repl-reset)))
(global-set-key (kbd "C-c q") (lambda () (interactive) (cider-init-system)))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("a8245b7cc985a0610d71f9852e9f2767ad1b852c2bdea6f4aadc12cce9c4d6d0" "45caeeb594422c20499f96b51c73f9bc04d666983d0a1d16f5b9f51a9ec874fa" "f024aea709fb96583cf4ced924139ac60ddca48d25c23a9d1cd657a2cf1e4728" "d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" default)))
 '(haskell-notify-p t)
 '(haskell-process-type (quote ghci))
 '(haskell-stylish-on-save t)
 '(haskell-tags-on-save t)
 '(js-curly-indent-offset 0)
 '(js-indent-level 2)
 '(magit-diff-options nil)
 '(sclang-auto-scroll-post-buffer t)
 '(sclang-eval-line-forward nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(diff-added ((t (:foreground "Green"))))
 '(diff-removed ((t (:foreground "Red"))))
 '(ediff-even-diff-A ((((class color) (background dark)) (:background "dark green"))))
 '(ediff-even-diff-B ((((class color) (background dark)) (:background "dark red"))))
 '(ediff-odd-diff-A ((((class color) (background dark)) (:background "dark green"))))
 '(ediff-odd-diff-B ((((class color) (background dark)) (:background "dark red"))))
 '(mumamo-background-chunk-major ((((class color) (background dark)) (:background "black"))))
 '(mumamo-background-chunk-submode1 ((((class color) (background dark)) (:background "black")))))

;;;;haskell
(add-hook 'haskell-mode-hook 'intero-mode)
(add-hook 'haskell-mode-hook 'haskell-indentation-mode)
(global-set-key (kbd "C-c i") 'haskell-indentation-mode)

;;ledger-cli
(add-to-list 'auto-mode-alist
             '("ledger.*\\.dat$" . ledger-mode))

(load-theme 'tsdh-dark t)
